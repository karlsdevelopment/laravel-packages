<?php

return [
    'urlNotFound' => 'The url :url was is not part of this API.',
    'modelNotFound' => 'Model(s) :model with the id(s) :ids does not exist.',
];
