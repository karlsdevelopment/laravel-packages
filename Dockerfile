FROM php:8.3-fpm-alpine AS klp-app
ARG USER_ID=1000
ARG GROUP_ID=1000
WORKDIR /var/www
RUN apk add --no-cache $PHPIZE_DEPS\
    linux-headers
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug
RUN docker-php-ext-install -j$(nproc) sockets

RUN mkdir -p /usr/local/etc/php/conf.d/
RUN addgroup --gid $GROUP_ID laravel
RUN adduser --uid $USER_ID --ingroup laravel -D --shell /bin/sh laravel

COPY ./docker/php/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN mkdir /opt/phpstorm-coverage
RUN chown -R laravel:laravel /opt/phpstorm-coverage
COPY . .
RUN chown -R laravel:laravel .
USER laravel
RUN php artisan storage:link
