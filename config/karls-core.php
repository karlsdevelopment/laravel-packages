<?php

use Symfony\Component\HttpFoundation\Response;

return [
    'httpDefaultSuccess' => Response::HTTP_OK,
    'httpDefaultFail' => Response::HTTP_BAD_REQUEST,
];
